<?php

/**
 * Реализация hook_theme
 */
function site_forms_api_f2_theme() {
    
  return array(
        
	'views_view_unformatted__completed_forms_admin' => array(
	  'base hook' => 'views_view_unformatted',
	  'path' => drupal_get_path('module', 'site_forms_api_f2') . '/templates',
	  'template' => 'views-view-unformatted--completed-forms-admin',
	),

	'node__completed_forms__teaser' => array(
	  'render element' => 'elements',
	  'base hook' => 'node',
	  'path' => drupal_get_path('module', 'site_forms_api_f2') . '/templates',
	  'template' => 'node--completed-forms--teaser',
	),
	  
	'node__completed_forms__full' => array(
	  'render element' => 'elements',
	  'base hook' => 'node',
	  'path' => drupal_get_path('module', 'site_forms_api_f2') . '/templates',
	  'template' => 'node--completed-forms--full',
	),
	  
  );
    
}



/**
 * Реализация hook_block_info
 */
function site_forms_api_f2_block_info() {
  
  //Блок c ссылка вызова форм
  $blocks['forms-links'] = array(
    'info' => 'Forms links (site_forms_api_f2)', 
    'cache' => DRUPAL_NO_CACHE,
  );
  
  return $blocks;
  
}  

/**
 * Реализация hook_block_view()
 */
function site_forms_api_f2_block_view($delta = '') {

  $block = array();

  switch ($delta) {
    case 'forms-links':
      $block['content'] = 'Блок со ссылками форм, требует шаблона';
      break;
  }
  
  return $block;
}
/**
 * массив названий форм
 * @return array
 */
function site_forms_api_f2_get_forms_title_list() {
    $options_forms_title = siteFormsApiBase::getFormsTitleArray();
    return $options_forms_title;
}

/**
 * Реализация form_alter
 * Изменение значений true/false в фильтре флага
 * для списка всех сообщений и списка сообщений по пользователям
 */
function site_forms_api_f2_form_alter(&$form, &$form_state, $form_id) {
    if(($form['#id'] == 'views-exposed-form-completed-forms-admin-page-allmessages')
        ||($form['#id'] == 'views-exposed-form-completed-forms-admin-page-messages')){
        
        $form['flagged']['#options'][0] = t('No');
        $form['flagged']['#options'][1] = t('Yes');
    }
}