<?php

function site_forms_api_f2_menu() {
    
  $items = array();

  // редактирование  раздела сообщений
  $items['adminmenu/messages'] = array(
    'title' => 'Messages',  
    'page callback' => 'pr_admin_f_menu_empty',
    'access arguments' => array('manage site_forms_api_f2 view my messages'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'menu-adminmenu',
    'expanded' => true,
    'weight' => '1'
  );
  // добавляем иконку
  pr_admin_f_api_adminmenu_icon_set('adminmenu/messages', 'fa fa-envelope-o');

  return $items;
}

/**
 * Реализация hook_menu_alter
 */
function site_forms_api_f2_menu_link_alter(&$item) {

  // подключение как подпункт
  if ((isset($item['path'])) and ($item['path'] == 'adminmenu/messages/my'))  {
    pr_admin_f_api_submenu_create($item, 'adminmenu/messages');
  }   
  if ((isset($item['path'])) and ($item['path'] == 'adminmenu/messages/all'))  {
    pr_admin_f_api_submenu_create($item, 'adminmenu/messages');
  }   
    
}

/* 
 * хуки и функции для создания меню в панели управления
 */

function site_forms_api_f2_permission() {
  return array(
	'administer site_forms_api_f2 view all messages' => array(        
      'title' => 'Просмотр всех сообщений',
	  'description' => 'Права доступа на просмотр "Всех сообщейний" '
    ),
    'manage site_forms_api_f2 view my messages' => array(        
      'title' => 'Просмотр моих сообщений',
	  'description' => 'Права доступа на просмотр "Мои сообщение"'
    ),
    'manage forms_api2 settings message' => array(        
      'title' => 'Настройка данных об отправки писем',
	  'description' => 'Право вносить настройки отправки сообщение (тема, текст письма)'
    ),  
  );
}
