<?php

class siteFormsApiBase {
  
  //имя модуля для вызова hook_mail()
  private $module_name = 'site_forms_api_f';
  //язык
  private $language;
  //сразу письма не отправялем
  private $send = FALSE;
  
  
  /**
  * Функция для отправки писем
  * 
  * @param type $key - ключ идентификации для hook_mail()
  * @param type $to - почта получателя name<email>
  * @param type $from - почта отправителя name<email>
  * @param type $param - массив array('subject' => Тема письма, 'body' => 'Тело письма')
  */
  public function sendMail($key, $to, $from, $param) {
	
	//получаем язык по умолчанию
    $this->language = language_default();
    
    $message = drupal_mail(
	  $this->module_name, 
	  $key, 
	  $to, 
	  $this->language, 
	  $param, 
	  $from, 
	  $this->send
	);
	
	//добавим к письму отображение html
    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8';
    $message['subject'] = $param['subject'];

    $message['body'] = array($param['body'], '');
    
    $system = drupal_mail_system($this->module_name, $key);
    $message = $system->format($message);
	
    //отправляем письмо
    $message['result'] = $system->mail($message);
    
    //если при отправки не возникло ошибок
    if(!$message['send']){
        //записываем в отчет что письмо не может быть доставлено
        watchdog('site_forms_api_f2', t('The message can not be sent, please contact the Site Administrator'));
    }
	
  }
  
  /**
   * Функция для создания материала формы
   * 
   * @param type $data - array(
   *  [FORM_NAME] => назвавание формы,
   *  [SENDER_EMAIL] => електронный адрес отправителя,
   *  [SENDER_NAME] => имя отправителя,
   *  [SENDER_PHONE] => телефон отправителя,
   *  [FORM_FILE] => изображение,
   *  [RECIPIENT_UID] => uid получателя
   *  [FORM_DATA] => остальны данные в html виде
   *  [FORM_ALL_DATA] => сериалайз данных форм
   * );
   */
  public function addForm($data){
    
    $node_forms = new stdClass();
    $node_forms->type = 'completed_forms';
	
	node_object_prepare($node_forms);
	
    $node_forms->language = LANGUAGE_NONE;
    $node_forms->status = 0;  
	//$node_forms->uid = $user->uid;     

    //имя формы   
    $node_forms->field_form_title[LANGUAGE_NONE][0]['value'] = $data['[FORM_NAME]'];    
    
    //получаем номер формы
    $query = db_select('field_data_field_form_number');
    $query->addExpression('MAX(field_form_number_value)');
    $max = $query->execute()->fetchField();
    $node_forms->field_form_number[LANGUAGE_NONE][0]['value'] = (int)$max+1;
    
    //e-mail
	$node_forms->field_form_email = array();
    if(!empty($data['[SENDER_EMAIL]'])){
	  $node_forms->field_form_email[LANGUAGE_NONE][0]['email'] = $data['[SENDER_EMAIL]'];
    }
    
    //имя
	$node_forms->field_form_name = array();
    if(!empty($data['[SENDER_NAME]'])){
	  $node_forms->field_form_name[LANGUAGE_NONE][0]['value'] = $data['[SENDER_NAME]'];
    }
    
    //телефон
	$node_forms->field_form_phone = array();
    if(!empty($data['[SENDER_PHONE]'])){
	  $node_forms->field_form_phone[LANGUAGE_NONE][0]['value'] = $data['[SENDER_PHONE]'];
    }
    
    //ip адрес
	if (!empty($_SERVER['HTTP_CLIENT_IP'])){
	  $ip = $_SERVER['HTTP_CLIENT_IP'];
    }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
	  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    }else{
	  $ip = $_SERVER['REMOTE_ADDR'];
    }
    $node_forms->field_form_ip[LANGUAGE_NONE][0]['value'] = $ip;

    //изображение
    if (!empty($data['[FORM_FILE]']) && is_array($data['[FORM_FILE]'])) {
	  foreach ($data['[FORM_FILE]'] as $file) {
		$node_forms->field_form_images[LANGUAGE_NONE][] = (array)$file;
	  }
    }

    //получатель
    if(!empty($data['[RECIPIENT_UID]'])){
	  $node_forms->field_form_recipient[LANGUAGE_NONE][0]['target_id'] = $data['[RECIPIENT_UID]'];
    }
	
	if(!empty($data['[FORM_DATA]'])){
	  $node_forms->field_form_human_data[LANGUAGE_NONE][0]['value'] = $data['[FORM_DATA]'];
	}else{
	  $node_forms->field_form_human_data[LANGUAGE_NONE][0]['value'] = t('it4r_all_admform_notmoreinfo');
	}
	
    //сериалайз
	if(!empty($data['[FORM_ALL_DATA]'])){
	  $node_forms->field_form_machines_data[LANGUAGE_NONE][0]['value'] = serialize($data['[FORM_ALL_DATA]']);
	}
    
    //дата/время
    $node_forms->field_form_date_send[LANGUAGE_NONE][0]['value'] = time();
    
    //сохраняем отчет
    node_save($node_forms);
	
  }
  
  static function getFormRecipient(){
	
	$options = array();
	
	//получаем всех пользователей
	$users = entity_load('user');
	
	foreach ($users as $uid => $user) {
	  if((user_access('manage forms_api2 settings message', $user)) && 
		($uid !== 1)){
		//если есть имя, фамилия - добавляем ее, если нет - добавляем логин
		if (isset($user->field_user_fio['und'])) {
		  $options[$uid] = $user->field_user_fio['und'][0]['value'];
		} else {
		  $options[$uid] = $user->name;
		}
	  }
	}
	
	return $options;
	
  }
  
  /**
   * массив названий форм
   * @return array
   */
  static function getFormsTitleArray() {
    //массив для значений
    $options_forms_title = array();
    //хук для изменения
    drupal_alter('forms_title_list', $options_forms_title);
    return $options_forms_title;
  }
  
}
