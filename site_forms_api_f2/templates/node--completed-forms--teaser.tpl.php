<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

  global $user;
  if($user->uid == 0){
	drupal_goto(variable_get('site_404'));
  }
  // номер формы
  $tpl_number = '';
  if (isset($content['field_form_number'])){
	$number_items = field_get_items('node', $node, 'field_form_number');
	$tpl_number = $number_items[0]['value'];
  }

  // дата
  $tpl_date = '';
  if (isset($content['field_form_date_send'])){
	$date_items = field_get_items('node', $node, 'field_form_date_send');
	$tpl_date = date('d.m.y H:i', $date_items[0]['value']);
  }  
    
  // заголовок формы
  $tpl_title = '';
  if (isset($content['field_form_title'])){
      $title_items = field_get_items('node', $node, 'field_form_title');
      $tpl_title = t($title_items[0]['value']);
  }  

  // флажок архива
  $tpl_flag = '';
  if (isset($content['flag_archive'])){
	$tpl_flag = $content['flag_archive']['#markup'];
  } 

  // ссылка удалить
  $destination = substr($_SERVER['REQUEST_URI'], 1);
  $tpl_delete_link = l(
		  '<i class="fa fa-times"></i> '.t('delete'), 
		  'node/'.$node->nid.'/delete', 
		  array(
			  'html'=>TRUE,
			  'query' => array('destination' => $destination))
		  );

  //имя
  $tpl_name = '';
  if (isset($content['field_form_name'])){
	$type_items = field_get_items('node', $node, 'field_form_name');
	$tpl_name = '<label>'.t('Name:').'</label><span>'.$type_items[0]['value'].'</span>';
  }

  //телефон
  $tpl_phone = '';
  if (isset($content['field_form_phone'])){
	$type_items = field_get_items('node', $node, 'field_form_phone');
	$tpl_phone = '<label>'.t('Phone:').'</label><span>'.$type_items[0]['value'].'</span>';
  }

  //почта
  $tpl_email = '';
  if (isset($content['field_form_email'])){
	$type_items = field_get_items('node', $node, 'field_form_email');
	$tpl_email = '<label>'.t('Mail:').'</label><span>'.$type_items[0]['email'].'</span>';
  }
  
  //ip
  $tpl_ip = '';
  if (isset($content['field_form_ip'])){
	$ip_items = field_get_items('node', $node, 'field_form_ip');
        $ip_link = l($ip_items[0]['value'], 'http://speed-tester.info/ip_location.php?ip='.$ip_items[0]['value'], array('attributes' => array('target' => '_blank')));
	$tpl_ip = '<label>'.t('ip:').'</label><span>'.$ip_link.'</span>';
  }

  // данные формы
  $tpl_data = '';
  if (isset($node->field_form_human_data['und'])){
	$sdata_items = field_get_items('node', $node, 'field_form_human_data');
	if($sdata_items){
	  $tpl_data = $sdata_items[0]['value'];
	}
  }
  
  // изображения
  $tpl_images = '';
  if (isset($content['field_form_images'])){
    $images_items = field_get_items('node', $node, 'field_form_images');
    for($i=0,$count = count($images_items);$i<$count;$i++){
        $tpl_image_uri = image_style_url('property-admin-mini', $images_items[$i]['uri']);
        $tpl_image_open_uri = file_create_url($images_items[$i]['uri']);
        $tpl_images .= '<div class="col-sm-4">';
          $tpl_images .= '<a href="'.$tpl_image_open_uri.'" target="_blank">';
            $tpl_images .= '<img src="'.$tpl_image_uri.'">';
          $tpl_images .= '</a>';
        $tpl_images .= '</div>';
    }
  }
  
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-md-10 list-inline">
                <strong>#<?php print $tpl_number; ?>. <?php print $tpl_title; ?></strong>
            </div>
            <div class="col-md-2 small">
                <?php print $tpl_date; ?>
            </div>
        </div>
    </div>
  <div class="panel-body">
    <div class="row">
        <div class="col-md-4"  id="forms-api-item-contacts">
            <p><?php print $tpl_name; ?></p>
            <p><?php print $tpl_phone; ?></p>
            <p><?php print $tpl_email; ?></p>
            <p><?php print $tpl_ip; ?></p>
        </div>
        <div class="col-md-6">
            <?php print $tpl_data; ?>
            <?php print $tpl_images; ?>
        </div>
        <div class="col-md-2">
            <div class="row">
                <div class="col-md-6 col-xs-3" id="forms-api-item-archive">
                    <?php print $tpl_flag; ?>архив
                </div>
                <div class="col-md-6 col-xs-3" id="forms-api-item-delete">
                    <?php 
                        if(node_access('delete', $node)){
                            print $tpl_delete_link;
                        }
                    ?>
                </div>   
            </div>
        </div>
    </div>
  </div>
</div>



