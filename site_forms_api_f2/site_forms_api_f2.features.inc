<?php
/**
 * @file
 * site_forms_api_f2.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function site_forms_api_f2_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function site_forms_api_f2_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function site_forms_api_f2_flag_default_flags() {
  $flags = array();
  // Exported flag: "archive".
  $flags['archive'] = array(
    'entity_type' => 'node',
    'title' => 'archive',
    'global' => 0,
    'types' => array(
      0 => 'completed_forms',
    ),
    'flag_short' => '<i class="fa fa-square-o"></i>',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => '<i class="fa fa-check-square-o"></i>',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'teaser' => 'teaser',
      'full' => 0,
      'rss' => 0,
      'PDF' => 0,
      'block' => 0,
      'block_micro' => 0,
      'block_mini' => 0,
      'infomaterial_block' => 0,
      'property_block' => 0,
      'property_block_mini' => 0,
      'property_slideshow' => 0,
      'revision' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'site_forms_api_f2',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

/**
 * Implements hook_node_info().
 */
function site_forms_api_f2_node_info() {
  $items = array(
    'completed_forms' => array(
      'name' => t('Заполненные формы'),
      'base' => 'node_content',
      'description' => t('Заполненные формы'),
      'has_title' => '1',
      'title_label' => t('Заголовок'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
