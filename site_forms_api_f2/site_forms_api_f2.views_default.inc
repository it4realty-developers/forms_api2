<?php
/**
 * @file
 * site_forms_api_f2.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function site_forms_api_f2_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'completed_forms_admin';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'completed_forms_admin';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Все заполненные формы';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['use_more_text'] = 'еще';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view adminmenu settings';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Применить';
  $handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Сбросить';
  $handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Сортировать по';
  $handler->display->display_options['exposed_form']['options']['sort_asc_label'] = 'Возр';
  $handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Убыв';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_label'] = 'Элементов на странице';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all_label'] = '- Все -';
  $handler->display->display_options['pager']['options']['tags']['first'] = '« первая';
  $handler->display->display_options['pager']['options']['tags']['previous'] = '‹ предыдущая';
  $handler->display->display_options['pager']['options']['tags']['next'] = 'следующая ›';
  $handler->display->display_options['pager']['options']['tags']['last'] = 'последняя »';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'node';
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'The site is not the completed form';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  /* Relationship: Flags: archive */
  $handler->display->display_options['relationships']['flag_content_rel']['id'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['table'] = 'node';
  $handler->display->display_options['relationships']['flag_content_rel']['field'] = 'flag_content_rel';
  $handler->display->display_options['relationships']['flag_content_rel']['required'] = 0;
  $handler->display->display_options['relationships']['flag_content_rel']['flag'] = 'archive';
  $handler->display->display_options['relationships']['flag_content_rel']['user_scope'] = 'any';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Flags: Flagged */
  $handler->display->display_options['filters']['flagged']['id'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['table'] = 'flagging';
  $handler->display->display_options['filters']['flagged']['field'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['relationship'] = 'flag_content_rel';
  $handler->display->display_options['filters']['flagged']['value'] = '0';
  $handler->display->display_options['filters']['flagged']['group'] = 1;
  $handler->display->display_options['filters']['flagged']['exposed'] = TRUE;
  $handler->display->display_options['filters']['flagged']['expose']['operator_id'] = '';
  $handler->display->display_options['filters']['flagged']['expose']['label'] = 'it4r_forms-api-f2_views_archive-title';
  $handler->display->display_options['filters']['flagged']['expose']['operator'] = 'flagged_op';
  $handler->display->display_options['filters']['flagged']['expose']['identifier'] = 'flagged';
  $handler->display->display_options['filters']['flagged']['expose']['required'] = TRUE;
  $handler->display->display_options['filters']['flagged']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
  );
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'completed_forms' => 'completed_forms',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Название формы (field_form_title) */
  $handler->display->display_options['filters']['field_form_title_value']['id'] = 'field_form_title_value';
  $handler->display->display_options['filters']['field_form_title_value']['table'] = 'field_data_field_form_title';
  $handler->display->display_options['filters']['field_form_title_value']['field'] = 'field_form_title_value';
  $handler->display->display_options['filters']['field_form_title_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_form_title_value']['expose']['operator_id'] = 'field_form_title_value_op';
  $handler->display->display_options['filters']['field_form_title_value']['expose']['label'] = 'it4r_forms-api-f2_views_form-title';
  $handler->display->display_options['filters']['field_form_title_value']['expose']['operator'] = 'field_form_title_value_op';
  $handler->display->display_options['filters']['field_form_title_value']['expose']['identifier'] = 'field_form_title_value';
  $handler->display->display_options['filters']['field_form_title_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
  );

  /* Display: All completed forms */
  $handler = $view->new_display('page', 'All completed forms', 'page_allmessages');
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'administer site_forms_api_f2 view all messages';
  $handler->display->display_options['path'] = 'adminmenu/messages/all';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'All messages';
  $handler->display->display_options['menu']['weight'] = '2';
  $handler->display->display_options['menu']['name'] = 'menu-adminmenu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  /* Display: My completed forms */
  $handler = $view->new_display('page', 'My completed forms', 'page_messages');
  $handler->display->display_options['defaults']['title'] = FALSE;
  $handler->display->display_options['title'] = 'Заполненные формы';
  $handler->display->display_options['defaults']['access'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'manage site_forms_api_f2 view my messages';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Получатель (field_form_recipient) */
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['id'] = 'field_form_recipient_target_id';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['table'] = 'field_data_field_form_recipient';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['field'] = 'field_form_recipient_target_id';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['exception']['title'] = 'Все';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['default_argument_type'] = 'current_user';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_form_recipient_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['path'] = 'adminmenu/messages/my';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'My messages';
  $handler->display->display_options['menu']['weight'] = '1';
  $handler->display->display_options['menu']['name'] = 'menu-adminmenu';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['completed_forms_admin'] = array(
    t('Master'),
    t('Все заполненные формы'),
    t('еще'),
    t('Применить'),
    t('Сбросить'),
    t('Сортировать по'),
    t('Возр'),
    t('Убыв'),
    t('Элементов на странице'),
    t('- Все -'),
    t('Offset'),
    t('« первая'),
    t('‹ предыдущая'),
    t('следующая ›'),
    t('последняя »'),
    t('The site is not the completed form'),
    t('flag'),
    t('it4r_forms-api-f2_views_archive-title'),
    t('it4r_forms-api-f2_views_form-title'),
    t('All completed forms'),
    t('more'),
    t('My completed forms'),
    t('Заполненные формы'),
    t('Все'),
  );
  $export['completed_forms_admin'] = $view;

  return $export;
}
